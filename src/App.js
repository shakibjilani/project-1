import React, { useEffect } from "react";
import Plotly from "plotly.js-dist";
import html2pdf from "html2pdf.js";
import "./App.css";

function App() {
  useEffect(() => {
    const feedbackValue = JSON.parse(
      document.getElementById("feedbackValue").value
    );
    const labels = feedbackValue.map((item) => item.question);
    const data = feedbackValue.map((item) => parseInt(item.value));

    // Create a plotly trace
    const trace = {
      y: labels,
      x: data,
      type: "bar",
      orientation: "h",
      marker: {
        color: "rgba(75, 192, 192, 0.2)",
        line: {
          color: "rgba(75, 192, 192, 1)",
          width: 1,
        },
      },
      text: labels,
      insidetextanchor: "start",
      insidetextfont: { size: 14 },
      textfont: { size: 14 },
    };

    // Create layout
    const layout = {
      margin: { t: 0 },
      xaxis: {
        title: "Values",
      },
      yaxis: {
        automargin: true,
        automargin_pad: 20,
      },
      displayModeBar: false,
    };

    // Create plotly chart
    Plotly.newPlot("barGraph", [trace], layout);

    const generatePdfBtn = document.getElementById("generatePdfBtn");

    // Add event listener to the button
    generatePdfBtn.addEventListener("click", function generatePdf() {
      const chartContainer = document.getElementById("barGraph");
      html2pdf().from(chartContainer).save();
    });

    // Remove the event listener when the component unmounts
    return () => {
      generatePdfBtn.removeEventListener("click", function generatePdf() {
        const chartContainer = document.getElementById("barGraph");
        html2pdf().from(chartContainer).save();
      });
    };
  }, []);

  return (
    <div className="App">
      <style>{`
        .modebar-container {
          display: none !important;
        }
      `}</style>
      <div style={{ padding: "100px" }}>
        <div id="barGraph" style={{ width: "400px", height: "400px" }}></div>
      </div>
      <button id="generatePdfBtn">Generate PDF</button>
      <input
        type="hidden"
        id="feedbackValue"
        value='[{"question":"Question 1","value":"5"},{"question":"Question 2","value":"8"},{"question":"Question 3","value":"3"},{"question":"Question 4","value":"6"}]'
      />
    </div>
  );
}

export default App;
